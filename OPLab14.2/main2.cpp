#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int main() {
	const char n = 5;
	const char m = 5;
	double max = 0, min = 100, average[5] = { 0 }, productOfMainDiag = 1, summaOfMainDiag = 0, summaOfElementsBelowMainDiagonal = 0;
	int maxij[2] = { 0, 0 }, minij[2] = {0, 0};


	srand(time(NULL));

	double b[n][m];
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			b[i][j] = -99 + rand() % 199 + (double) (rand() % 100) / 100;
			printf("%6.2lf ", b[i][j]);
			if (b[i][j] > max) {
				max = b[i][j];
				maxij[0] = i;
				maxij[1] = j;
			}
			else if (b[i][j] < min) {
				min = b[i][j];
				minij[0] = i;
				minij[1] = j;
			}

			average[i] += b[i][j];
		}
		average[i] /= 5;;
		printf("\n");
	}

	for (int i = 0; i < n; i++) {
		productOfMainDiag *= b[i][i];
		summaOfMainDiag += b[i][i];
	}

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < i; j++) summaOfElementsBelowMainDiagonal += b[i][j];
	}

	printf("\nMin element = b[%d][%d] = %.2lf", minij[0], minij[1], min);
	
	printf("\nMax element = b[%d][%d] = %.2lf", maxij[0], maxij[1], max);

	printf("\nAverage:");
	for (int i = 0; i < n; i++) printf("\n%.2lf", average[i]);

	printf("\nProduct of main diagonal's elements: %lf", productOfMainDiag);

	printf("\nSumma of main diagonal's elements: %.2lf", summaOfMainDiag);

	printf("\nSumma of elements below main diagonal: %.2lf", summaOfElementsBelowMainDiagonal);

	return 0;
}
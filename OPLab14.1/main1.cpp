#include <stdio.h>

int main() {
	int matrix[4][4];
	for (int i = 0; i <= 3; i++) {
		for (int j = 0; j <= 3; j++) {
			matrix[i][j] = i * j - 3;
			printf("%3d ", matrix[i][j]);
		}
		printf("\n");
	}

	int product = 1;
	for (int i = 0; i <= 3; i++) {
		product *= matrix[i][2];
	}
	printf("\n%d", product);

	return 0;
}
#define _CRT_SECURE_NO_WARNINGS
#define GREEN "\033[0;32m"
#define NORMAL "\033[0m"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	srand(time(NULL));

	int arr[6][5], max2 = 0, max4 = 0;

	printf("     �������� �      | 1  | 2  | 3  | 4  | 5  |\n");

	for (int k = 0; k < 6; k++) {
		printf("��������� ����� �%d -" GREEN " | ", k + 1);
		for (int n = 0; n < 5; n++) {
			arr[k][n] = rand() % (10 * 4) + 50;
			printf("%2d | ", arr[k][n]);
		}
		printf(NORMAL "\n");
	}

	for (int k = 1; k < 6; k++) {
		if (arr[k][1] > arr[max2][1]) max2 = k;
		if (arr[k][3] > arr[max4][3]) max4 = k;
	}

	printf("\n������ �������� ������ ����������� ������� ������ � %d-�� ���������� �����.\n", max2 + 1);
	printf("��������� �������� ������ ����������� ������� ������ � %d-�� ���������� �����.\n", max4 + 1);

	return 0;
}
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {

	srand(time(NULL));

	const int N = 10;
	const int M = 20;

	int n, m;
	int a, b;

	int nim1[N][M], nim2[N][M], nim3[N][M];

	do {
		printf("n = ");
		scanf("%d", &n);
	} while (n <= 0 || n >= N);
	do {
		printf("m = ");
		scanf("%d", &m);
	} while (m <= 0 || m >= M);

	printf("a = ");
	scanf("%d", &a);
	do {
		printf("b (b>a) = ");
		scanf("%d", &b);
	} while (b <= a);

	printf("\nnim1 = \n");
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			nim1[i][j] = a + rand() % (b - a + 1);
			printf("%3d ", nim1[i][j]);
		}
		printf("\n");
	}	
	printf("\nnim2 = \n");

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			nim2[i][j] = a + rand() % (b - a + 1);
			printf("%3d ", nim2[i][j]);
		}
		printf("\n");
	}

	printf("\nnim1 + nim2 = \n");

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			nim3[i][j] = nim1[i][j] + nim2[i][j];
			printf("%3d ", nim3[i][j]);
		}
		printf("\n");
	}

	return 0;
}
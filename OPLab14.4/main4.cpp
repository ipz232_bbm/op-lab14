#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {

	srand(time(NULL));

	const int n = 10;

	int matrix[n][n];

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			matrix[i][j] = -100 + rand() % 200;
			printf("%4d ", matrix[i][j]);
		}
		printf("\n");
	}
	int min = matrix[0][0];
	int minij[] = { 0, 0 };
	for (int i = 0; i < n; i++) {
		for (int j = n - 1; i+j >= n - 1; j--) {
			if (matrix[i][j] < min) {
				min = matrix[i][j];
				minij[0] = i;
				minij[1] = j;
			}
		}
	}

	printf("Min element = matrix[%d][%d] = %d", minij[0], minij[1], min);

	return 0;
}